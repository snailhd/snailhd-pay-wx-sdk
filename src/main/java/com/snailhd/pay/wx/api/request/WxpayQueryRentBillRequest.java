package com.snailhd.pay.wx.api.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.snailhd.pay.wx.api.AbstractWxpayRentBillRequest;
import com.snailhd.pay.wx.api.WxpayApiAddress;
import com.snailhd.pay.wx.api.WxpayRequest;
import com.snailhd.pay.wx.api.response.WxpayQueryRentBillResponse;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author hd
 * @description
 * @date 2019-08-11 09:47
 */

@Getter
@Setter
@NoArgsConstructor
public class WxpayQueryRentBillRequest extends AbstractWxpayRentBillRequest<WxpayQueryRentBillResponse> {

    /**商户服务订单号*/
    @JSONField(name = "out_order_no")
    private String outOrderNo;

    /**回跳查询id*/
    @JSONField(name = "return_query_id")
    private String returnQueryId;

    /**
     * 获取请求地址
     *
     * @return 请求地址
     */
    @Override
    public String gainApiRequestAddr() {
        return WxpayApiAddress.QUERY_RENT_BILL;
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<WxpayQueryRentBillResponse> gainResponseClass() {
        return WxpayQueryRentBillResponse.class;
    }

    @Override
    public String gainLogPre() {
        return "微信支付分查询";
    }
}
