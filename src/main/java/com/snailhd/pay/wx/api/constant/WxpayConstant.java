package com.snailhd.pay.wx.api.constant;

/**
 * @author hd
 * @description
 * @date 2019-11-13 18:53
 */
public interface WxpayConstant {
    /**
     * 业务类型
     */
    String BUSINESS_TYPE = "wxpayScoreUse";
}
