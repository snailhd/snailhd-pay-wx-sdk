package com.snailhd.pay.wx.api.response;

import com.alibaba.fastjson.annotation.JSONField;
import com.snailhd.pay.wx.api.WxpayApiCode;
import com.snailhd.pay.wx.api.WxpayResponse;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author hd
 * @description 下单返回参数
 * @date 2019-08-09 10:04
 */

@Setter
@Getter
@NoArgsConstructor
public class WxpayUnifiedOrderResponse extends WxpayResponse {

    /**交易类型*/
    @JSONField(name="trade_type")
    private String  tradeType;

    /**预支付交易会话标识*/
    @JSONField(name="prepay_id")
    private String  prepayId;

    /**二维码链接*/
    @JSONField(name="code_url")
    private String  codeUrl;

    /**支付跳转链接*/
    @JSONField(name = "mweb_url")
    private String mwebUrl;

    /**
     * 交易是否成功
     *
     * @return
     */
    @Override
    public boolean isSuccess() {
        return WxpayApiCode.RESULT_CODE_SUCCESS.equals(this.getResultCode());
    }
}
