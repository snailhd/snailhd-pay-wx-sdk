package com.snailhd.pay.wx.api;

/**
 * @author hd
 * @description
 * @date 2019-08-08 15:10
 */
public interface WxpayClient {

    public <T extends WxpayResponse> T executeRentBill(AbstractWxpayRentBillRequest<T> request) throws WxApiException;

    public <T extends WxpayResponse> T execute(WxpayRequest<T> request) throws WxApiException;

    public <T extends WxpayResponse> T execute(WxpayRequest<T> request,Integer exeNum) throws WxApiException;

}
