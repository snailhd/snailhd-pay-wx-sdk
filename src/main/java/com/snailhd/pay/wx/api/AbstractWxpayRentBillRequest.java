package com.snailhd.pay.wx.api;

import com.alibaba.fastjson.annotation.JSONField;
import com.snailhd.pay.wx.api.WxpayRequest;
import lombok.Data;

/**
 * @author hd
 * @description
 * @date 2019-08-11 12:20
 */

@Data
public abstract class AbstractWxpayRentBillRequest<T extends WxpayResponse> extends WxpayRequest<T> {

    /**接口版本号*/
    private String version;

    /**服务ID*/
    @JSONField(name = "service_id")
    private String serviceId;
}
