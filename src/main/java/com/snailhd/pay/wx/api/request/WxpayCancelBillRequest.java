package com.snailhd.pay.wx.api.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.snailhd.pay.wx.api.AbstractWxpayRentBillRequest;
import com.snailhd.pay.wx.api.WxpayApiAddress;
import com.snailhd.pay.wx.api.response.WxpayCancelBillResponse;
import lombok.*;

/**
 * @author hd
 * @date 2019-08-11 10:00
 */

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class WxpayCancelBillRequest extends AbstractWxpayRentBillRequest<WxpayCancelBillResponse> {

    /**商户服务订单号*/
    @JSONField(name = "out_order_no")
    private String outOrderNo;

    /**撤销原因*/
    private String reason;


    /**
     * 获取请求地址
     *
     * @return 请求地址
     */
    @Override
    public String gainApiRequestAddr() {
        return WxpayApiAddress.CANCEL_BILL;
    }

    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<WxpayCancelBillResponse> gainResponseClass() {
        return WxpayCancelBillResponse.class;
    }

    @Override
    public String gainLogPre() {
        return "微信支付分撤销";
    }
}
