package com.snailhd.pay.wx.api;

/**
 * @author hd
 * @description
 * @date 2019-08-11 23:29
 */
public interface WxpayResponseErrCode {

    String SYSTEMERROR = "SYSTEMERROR";

    String NOT_EXISTS = "NOT_EXISTS";

}
