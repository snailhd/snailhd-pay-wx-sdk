package com.snailhd.pay.wx.api.vo;

import com.snailhd.pay.wx.api.constant.WxpayConstant;
import com.snailhd.pay.wx.api.response.WxpayCreateRentBillResponse;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author hd
 * @description
 * @date 2019-10-30 12:20
 */

@Data
@NoArgsConstructor
public class WxScorePullUpPayConsoleVO implements Serializable {

    private String businessType;
    private WxScorePullUpPayConsoleExtraData extraData;

    public WxScorePullUpPayConsoleVO(WxpayCreateRentBillResponse response, String key) throws Exception {
        this.businessType = WxpayConstant.BUSINESS_TYPE;
        this.extraData = new WxScorePullUpPayConsoleExtraData(response, key);
    }
}
