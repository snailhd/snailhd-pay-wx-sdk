package com.snailhd.pay.wx.api.notify;

import com.alibaba.fastjson.annotation.JSONField;
import com.snailhd.pay.wx.api.WxpayApiCode;
import com.snailhd.pay.wx.api.WxpayResponse;
import com.snailhd.pay.wx.api.util.AESGcmUtil;
import com.snailhd.pay.wx.api.util.AESUtil;
import com.snailhd.pay.wx.api.util.XMLParser;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
 * @author hd
 * @description
 * @date 2019-08-09 23:32
 */

@Getter
@Setter
@NoArgsConstructor
@ToString
public class WxpayRefundNotify implements Serializable {

    @JSONField(name = "return_code")
    private String returnCode;

    @JSONField(name = "return_msg")
    private String return_msg;

    @JSONField(name="appid")
    private String appid;

    @JSONField(name="mch_id")
    private String mchId;

    @JSONField(name="nonce_str")
    private String nonceStr;

    @JSONField(name="req_info")
    private String reqInfo ;

    private WxpayRefundNotifyPlaintext notifyPlaintext;

    /**
     * 解密密文
     * @return
     */
    public WxpayRefundNotifyPlaintext decrypt(String key) throws Exception {
        if(!isReqSuccess()){
            return null;
        }
        String text = AESUtil.decryptData(this.reqInfo,key);
        notifyPlaintext = XMLParser.getObjFromXML(text, WxpayRefundNotifyPlaintext.class);
        return notifyPlaintext;
    }

    public boolean isReqSuccess(){
        return (StringUtils.isNotEmpty(this.returnCode) && WxpayApiCode.RETURN_CODE_SUCCESS.equals(this.returnCode));
    }

}
