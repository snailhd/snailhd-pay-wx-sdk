package com.snailhd.pay.wx.api.notify;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author hd
 * @description
 * @date 2019-08-09 23:25
 */
@Setter
@Getter
@NoArgsConstructor
public class WxpayNotifyReturn {


    @JSONField(name="return_code")
    private String returnCode;

    @JSONField(name="return_msg")
    private String returnMsg;

    public WxpayNotifyReturn(String returnCode, String returnMsg) {
        this.returnCode = returnCode;
        this.returnMsg = returnMsg;
    }

    public String toJSONString() {
        return JSON.toJSONString(this);
    }

    public String successText(){
        return new WxpayNotifyReturn("SUCCESS","OK").toJSONString();
    }

    public String failText(){
        return new WxpayNotifyReturn("FAIL","").toJSONString();
    }
    public String failText(String returnMsg){
        return new WxpayNotifyReturn("FAIL",returnMsg).toJSONString();
    }
}
