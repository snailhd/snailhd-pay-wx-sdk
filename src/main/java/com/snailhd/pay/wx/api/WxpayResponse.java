package com.snailhd.pay.wx.api;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author hd
 * @description
 * @date 2019-08-08 15:11
 */

@Setter
@Getter
@NoArgsConstructor
public abstract class WxpayResponse implements Serializable {

    @JSONField(name="return_code")
    private String returnCode;

    @JSONField(name="return_msg")
    private String returnMsg;


    @JSONField(name="result_code")
    private String  resultCode;

    @JSONField(name="err_code")
    private String  errCode;

    @JSONField(name="err_code_des")
    private String  errCodeDes;

    private String   sign;

    private String  appid;

    @JSONField(name="mch_id")
    private String   mchId;

    @JSONField(name="device_info")
    private String   deviceInfo;

    @JSONField(name="nonce_str")
    private String   nonceStr;



    /**
     * 交易是否成功
     * @return
     */
    public boolean isSuccess(){
        return WxpayApiCode.RETURN_CODE_SUCCESS.equals(returnCode) && WxpayApiCode.RESULT_CODE_SUCCESS.equals(resultCode);
    };

}
