package com.snailhd.pay.wx.api.vo;

import com.alibaba.fastjson.annotation.JSONField;
import com.snailhd.pay.wx.api.response.WxpayCreateRentBillResponse;
import com.snailhd.pay.wx.api.util.WxpaySignature;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author hd
 * @date 2019-10-30 12:21
 */

@Setter
@Getter
@ToString
@NoArgsConstructor
public class WxScorePullUpPayConsoleExtraData implements Serializable {

    @JSONField(name = "mch_id")
    private String mchId;

    private String timestamp;

    @JSONField(name = "nonce_str")
    private String nonceStr;

    @JSONField(name = "sign_type")
    private String signType;

    private String sign;

    @JSONField(name = "package")
    private String packageAlia;


    public WxScorePullUpPayConsoleExtraData(WxpayCreateRentBillResponse response, String key) throws Exception {
        this.mchId = response.getMchId();
        this.packageAlia = response.getPackageStr();
        this.nonceStr = response.getNonceStr();
        this.timestamp = (System.currentTimeMillis() / 1000 + "");
        this.signType = WxpaySignature.SIGN_TYPE_HMAC_SHA256;
        this.sign = WxpaySignature.getSign(this,key, WxpaySignature.SIGN_TYPE_HMAC_SHA256);
    }
}
