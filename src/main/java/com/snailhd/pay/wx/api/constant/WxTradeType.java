package com.snailhd.pay.wx.api.constant;

/**
 * @author hd
 * @description
 * @date 2019-11-18 16:40
 */
public interface WxTradeType {

    /**
     * 小程序支付，js支付
     */
    String JSAPI = "JSAPI";
}
