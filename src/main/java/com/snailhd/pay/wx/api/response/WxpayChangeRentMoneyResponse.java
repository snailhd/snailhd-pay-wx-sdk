package com.snailhd.pay.wx.api.response;

import com.alibaba.fastjson.annotation.JSONField;
import com.snailhd.pay.wx.api.WxpayResponse;
import lombok.Data;

/**
 * @author hd
 * @description
 * @date 2019-08-11 10:14
 */

@Data
public class WxpayChangeRentMoneyResponse extends WxpayResponse {

    /**商户服务订单号*/
    @JSONField(name = "out_order_no")
    private String outOrderNo;

    /**服务ID*/
    @JSONField(name = "service_id")
    private String serviceId;

    /**微信支付服务订单号*/
    @JSONField(name = "order_id")
    private String orderId;

}
