package com.snailhd.pay.wx.api;

import lombok.Getter;
import lombok.Setter;

/**
 * @author hd
 * @description
 * @date 2019-08-08 15:33
 */

@Getter
@Setter
public class DefaultWxpayClient extends AbstractWxpayClient {


    public DefaultWxpayClient(String appid, String mchId, String key, String certPath, String certPassword, String apiv3key) {
        this.setAppid(appid);
        this.setMchId(mchId);
        this.setKey(key);
        this.setCertPath(certPath);
        this.setCertPassword(certPassword);
        this.setApiv3key(apiv3key);
    }

    public DefaultWxpayClient(String appid, String mchId, String key, String certPath, String certPassword, String apiv3key, String serviceId,String version) {
        this.setAppid(appid);
        this.setMchId(mchId);
        this.setKey(key);
        this.setCertPath(certPath);
        this.setCertPassword(certPassword);
        this.setApiv3key(apiv3key);
        this.setServiceId(serviceId);
        this.setVersion(version);
    }

}
