package com.snailhd.pay.wx.api.vo;

import com.alibaba.fastjson.annotation.JSONField;
import com.snailhd.pay.wx.api.response.WxpayUnifiedOrderResponse;
import com.snailhd.pay.wx.api.util.WxpaySignature;
import lombok.*;

import java.io.Serializable;

/**
 * @author hd
 * @description
 * @date 2019-09-16 14:48
 */
@Setter
@Getter
@ToString
@NoArgsConstructor
public class WxpayAppPullUpPayVO implements Serializable {

    private String appid;
    private String partnerid;
    private String prepayid;
    @JSONField(name="package")
    private String packageAlia;
    private String noncestr;
    private String timestamp;
    private String sign;

    public WxpayAppPullUpPayVO(WxpayUnifiedOrderResponse response, String appid,String merid,String key) throws Exception {
        this.appid = appid;
        this.partnerid = merid;
        this.prepayid = response.getPrepayId();
        this.packageAlia = "Sign=WXPay";
        this.noncestr = response.getNonceStr();
        this.timestamp = String.valueOf(System.currentTimeMillis() / 1000);
        this.sign = WxpaySignature.getSign(this,key, WxpaySignature.SIGN_TYPE_MD5);
    }

}
