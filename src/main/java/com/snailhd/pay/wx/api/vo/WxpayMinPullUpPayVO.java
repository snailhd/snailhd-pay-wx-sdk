package com.snailhd.pay.wx.api.vo;

import com.alibaba.fastjson.annotation.JSONField;
import com.snailhd.pay.wx.api.response.WxpayUnifiedOrderResponse;
import com.snailhd.pay.wx.api.util.WxpaySignature;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.security.Signature;
import java.util.HashMap;
import java.util.Map;

/**
 * @author hd
 * @description
 * @date 2019-10-31 15:04
 */

@Data
@NoArgsConstructor
public class WxpayMinPullUpPayVO implements Serializable {


    private String timeStamp;
    private String nonceStr;
    @JSONField(name="package")
    private String packageAlia;
    private String signType;
    private String paySign;


    public WxpayMinPullUpPayVO(WxpayUnifiedOrderResponse resp,String appid,String key)throws Exception {
        this.timeStamp = System.currentTimeMillis() / 1000+"";
        this.nonceStr = resp.getNonceStr();
        this.packageAlia = "prepay_id="+ resp.getPrepayId();
        this.signType = WxpaySignature.SIGN_TYPE_MD5;

        Map map = new HashMap();
        map.put("timeStamp", timeStamp);
        map.put("nonceStr", nonceStr);
        map.put("package", packageAlia);
        map.put("signType",signType);
        map.put("appId", appid);
        this.paySign = WxpaySignature.getSign(map,key,signType);
    }
}
