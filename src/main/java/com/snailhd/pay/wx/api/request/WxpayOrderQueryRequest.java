package com.snailhd.pay.wx.api.request;

import com.alibaba.fastjson.annotation.JSONField;
import com.snailhd.pay.wx.api.WxpayApiAddress;
import com.snailhd.pay.wx.api.WxpayRequest;
import com.snailhd.pay.wx.api.response.WxpayOrderQueryResponse;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

/**
 * @author hd
 * @description 查询订单 请求
 * @date 2019-08-09 13:19
 */

@Getter
@Setter
@NoArgsConstructor
public class WxpayOrderQueryRequest extends WxpayRequest<WxpayOrderQueryResponse> {


    @JSONField(name = "transaction_id")
    private String transactionId;

    @JSONField(name = "out_trade_no")
    private String outTradeNo;


    /**
     * 获取请求地址
     *
     * @return
     */
    @Override
    public String gainApiRequestAddr() {
        return WxpayApiAddress.ORDER_QUERY;
    }


    /**
     * 得到当前API的响应结果类型
     *
     * @return 响应类型
     */
    @Override
    public Class<WxpayOrderQueryResponse> gainResponseClass() {
        return WxpayOrderQueryResponse.class;
    }

    @Override
    public String gainLogPre() {
        return "微信支付查询";
    }
}
